
/* ========================================================
*
* Londinium - premium responsive admin template
*
* ========================================================
*
* File: application_blank.js;
* Description: Minimum of necessary js code for blank page.
* Version: 1.0
*
* ======================================================== */

/* # Data tables
================================================== */


	//===== Default datatable =====//

	function tabeldata(controller, orderby, ascdesc, orderfalse) {
		$('#serverside').dataTable({
			"bJQueryUI": false,
			"bAutoWidth": false,
			"sPaginationType": "full_numbers",
			"sDom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
			"oLanguage": {
				"sProcessing":   "Sedang memproses...",
				"sLengthMenu":   "<span>Tampilkan data:</span> _MENU_",
				"sZeroRecords":  "Tidak ditemukan data yang sesuai",
				"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
				"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
				"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
				"sInfoPostFix":  "",
				"sSearch":       "<span>Pencarian:</span> _INPUT_",
				"sUrl":          "",
				"oPaginate": {
					"sFirst":    "Pertama",
					"sPrevious": "<",
					"sNext":     ">",
					"sLast":     "Terakhir"
				}
			},

			"order": [[ orderby, ascdesc ]],
	      	"columnDefs": [{
	        	"targets": orderfalse,
	        	"orderable": false
	      	}],
	      	"processing": true,
	      	"serverSide": true,
	      	"ajax":{
		        url : controller+"/serverside",
		        type: "post",
		        error: function(data, err){ 
	          		console.log(data);
	          		alert(err);
	          		$(".serverside-error").html("");
	          		$("#serverside").append('<tbody><tr><th colspan="6">Tidak ditemukan data.</th></tr></tbody>');
	          		$("#serverside_processing").css("display","none");
	        	}
	      	}
	    });
	}

	function tabeldata_laporan(controller, orderby, ascdesc, data) {
		var dtable = $('#serverside').DataTable({
			"bJQueryUI": false,
			"bAutoWidth": false,
			"sPaginationType": "full_numbers",
			"sDom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
			"oLanguage": {
				"sProcessing":   "Sedang memproses...",
				"sLengthMenu":   "<span>Tampilkan data:</span> _MENU_",
				"sZeroRecords":  "Tidak ditemukan data yang sesuai",
				"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
				"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
				"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
				"sInfoPostFix":  "",
				"sSearch":       "<span>Pencarian:</span> _INPUT_",
				"sUrl":          "",
				"oPaginate": {
					"sFirst":    "Pertama",
					"sPrevious": "<",
					"sNext":     ">",
					"sLast":     "Terakhir"
				}
			},

			"order": [[ orderby, ascdesc ]],
	      	"processing": true,
	      	"serverSide": true,
	      	"ajax":{
		        url : controller+"/serverside",
		        type: "post",
		        "data": function ( d ) {
	                d.cari = data;
	            },
		        error: function(data, err){ 
	          		console.log(data);
	          		alert(err);
	          		$(".serverside-error").html("");
	          		$("#serverside").append('<tbody><tr><th colspan="6">Tidak ditemukan data.</th></tr></tbody>');
	          		$("#serverside_processing").css("display","none");
	        	}
	      	},
	    });

	    // Grab the datatables input box and alter how it is bound to events
		$(".dataTables_filter input")
		    .unbind() // Unbind previous default bindings
		    .bind("input", function(e) { // Bind our desired behavior
		        // If the length is 3 or more characters, or the user pressed ENTER, search
		        if(this.value.length >= 3 || e.keyCode == 13) {
		            // Call the API search function
		            dtable.search(this.value).draw();
		        }
		        // Ensure we clear the search if they backspace far enough
		        if(this.value == "") {
		            dtable.search("").draw();
		        }
		        return;
		    });
	}

	function crud_delete(code, controller, title) {
	    swal({   
	            title: "Apakah Anda yakin?",   
	            text: "Hapus "+title+"!",
	            type: "warning",   
	            showCancelButton: true,   
	            cancelButtonText: "Batal",   
	            confirmButtonColor: "#DD6B55",   
	            confirmButtonText: "Ya!",   
	            closeOnConfirm: false 
	        }, function(){   
	                window.location.href = controller+"/delete/"+code;
	      });
	  }

$(function() {

/* # tinymce
================================================== */

	// $('#serverside').dataTable({
	// 	"bJQueryUI": false,
	// 	"bAutoWidth": false,
	// 	"sPaginationType": "full_numbers",
	// 	"sDom": '<"datatable-header"fl><"datatable-scroll"t><"datatable-footer"ip>',
	// 	"oLanguage": {
	// 		"sProcessing":   "Sedang memproses...",
	// 		"sLengthMenu":   "<span>Tampilkan data:</span> _MENU_",
	// 		"sZeroRecords":  "Tidak ditemukan data yang sesuai",
	// 		"sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
	// 		"sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
	// 		"sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
	// 		"sInfoPostFix":  "",
	// 		"sSearch":       "<span>Pencarian:</span> _INPUT_",
	// 		"sUrl":          "",
	// 		"oPaginate": {
	// 			"sFirst":    "Pertama",
	// 			"sPrevious": "<",
	// 			"sNext":     ">",
	// 			"sLast":     "Terakhir"
	// 		}
	// 	},

	// 	"order": [[ 0, "desc" ]],
 //      	"columnDefs": [{
 //        	"targets": 2,
 //        	"orderable": false
 //      	}],
 //      	"processing": true,
 //      	"serverSide": true,
 //      	"ajax":{
	//         url : "provinsi/serverside",
	//         type: "post",
	//         error: function(data, err){ 
 //          		console.log(data);
 //          		alert(err);
 //          		$(".serverside-error").html("");
 //          		$("#serverside").append('<tbody><tr><th colspan="6">Tidak ditemukan data.</th></tr></tbody>');
 //          		$("#serverside_processing").css("display","none");
 //        	}
 //      	}
 //    });

/*	tinymce.init({ 
		selector:'#content',
		theme: 'modern',
		height: 300,
		plugins: [
		  'advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker',
		  'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
		  'save table contextmenu directionality emoticons template paste textcolor'
		],
		toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons'
	});
*/

/* # Bootstrap Plugins
================================================== */


	//===== Add fadeIn animation to dropdown =====//

	$('.dropdown, .btn-group').on('show.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).fadeIn(100);
	});


	//===== Add fadeOut animation to dropdown =====//

	$('.dropdown, .btn-group').on('hide.bs.dropdown', function(e){
		$(this).find('.dropdown-menu').first().stop(true, true).fadeOut(100);
	});


	//===== Prevent dropdown from closing on click =====//

	$('.popup').click(function (e) {
		e.stopPropagation();
	});






/* # Interface Related Plugins
================================================== */


	//===== DateRangePicker plugin =====// 

	$('#reportrange').daterangepicker(
	{
		startDate: moment().subtract('days', 29),
		endDate: moment(),
		minDate: '01/01/2012',
		maxDate: '12/31/2014',
		dateLimit: { days: 60 },
		ranges: {
		'Today': [moment(), moment()],
		'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
		'Last 7 Days': [moment().subtract('days', 6), moment()],
		'This Month': [moment().startOf('month'), moment().endOf('month')],
		'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
	},
	opens: 'left',
	buttonClasses: ['btn'],
	applyClass: 'btn-small btn-info btn-block',
	cancelClass: 'btn-small btn-default btn-block',
	format: 'MM/DD/YYYY',
	separator: ' to ',
	locale: {
		applyLabel: 'Submit',
		fromLabel: 'From',
		toLabel: 'To',
		customRangeLabel: 'Custom Range',
		daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
		monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
		firstDay: 1
		}
	},
	function(start, end) {
		$.jGrowl('A date range was changed', { header: 'Update', position: 'center', life: 1500 });
		$('#reportrange .date-range').html(start.format('<i>D</i> <b><i>MMM</i> <i>YYYY</i></b>') + '<em> - </em>' + end.format('<i>D</i> <b><i>MMM</i> <i>YYYY</i></b>'));
	}
	);
	
	/* Custom date display layout */
	$('#reportrange .date-range').html(moment().subtract('days', 29).format('<i>D</i> <b><i>MMM</i> <i>YYYY</i></b>') + '<em> - </em>' + moment().format('<i>D</i> <b><i>MMM</i> <i>YYYY</i></b>'));
	$('#reportrange').on('show', function(ev, picker) {
	  $('.range').addClass('range-shown');
	});

	$('#reportrange').on('hide', function(ev, picker) {
	  $('.range').removeClass('range-shown');
	});


	//===== jGrowl notifications defaults =====//

//	$.jGrowl.defaults.closer = false;
//	$.jGrowl.defaults.easing = 'easeInOutCirc';


	//===== Collapsible navigation =====//
	
/*	$('.sidebar-wide li:not(.disabled) .expand, .sidebar-narrow .navigation > li ul .expand').collapsible({
		defaultOpen: 'second-level,third-level',
		cssOpen: 'level-opened',
		cssClose: 'level-closed',
		speed: 150
	});

*/



/* # Default Layout Options
================================================== */

	//===== Hiding sidebar =====//

	$('.sidebar-toggle').click(function () {
		$('.page-container').toggleClass('sidebar-hidden');
	});


	//===== Disabling main navigation links =====//

	$('.navigation li.disabled a, .navbar-nav > .disabled > a').click(function(e){
		e.preventDefault();
	});

/* # Select2 dropdowns 
================================================== */

	
	//===== Datatable select =====//

	$(".dataTables_length select").select2({
		minimumResultsForSearch: "-1"
	});


	//===== Default select =====//

	$(".select").select2({
		minimumResultsForSearch: "-1",
		width: 200
	});


	//===== Liquid select =====//

	$(".select-liquid").select2({
		minimumResultsForSearch: "-1",
		width: "off"
	});


	//===== Full width select =====//

	$(".select-full").select2({
		minimumResultsForSearch: "-1",
		width: "100%"
	});


	//===== Select with filter input =====//

	$(".select-search").select2({
		width: "100%"
	});


	//===== Multiple select =====//

	$(".select-multiple").select2({
		width: "100%"
	});

	//===== Flexdatalist select =====//

	// $("#flexdatalist").flexdatalist({
	//     minLength: 1,
	//     selectionRequired: true,
	//     visibleProperties: ["nik","nama_lengkap"],
	//     searchIn: 'nik',
	//     data: 'countries.json'
	// });

	
	//===== Loading data select =====//

	$("#loading-data").select2({
		placeholder: "Enter at least 1 character",
        allowClear: true,
        minimumInputLength: 1,
        query: function (query) {
            var data = {results: []}, i, j, s;
            for (i = 1; i < 5; i++) {
                s = "";
                for (j = 0; j < i; j++) {s = s + query.term;}
                data.results.push({id: query.term + i, text: s});
            }
            query.callback(data);
        }
    });		


	//===== Select with maximum =====//

	$(".maximum-select").select2({ 
		maximumSelectionSize: 3, 
		width: "100%" 
	});		


	//===== Allow clear results select =====//

	$(".clear-results").select2({
	    placeholder: "Select a State",
	    allowClear: true,
	    width: 200
	});

	$(".select-clear").select2({
	    allowClear: true,
	    width: 200
	});


	//===== Select with minimum =====//

	$(".minimum-select").select2({
        minimumInputLength: 2,
        width: 200
    });


	//===== Multiple select with minimum =====//

	$(".minimum-multiple-select").select2({
        minimumInputLength: 2,
        width: "100%" 
    });

	
	//===== Disabled select =====//

	$(".select-disabled").select2(
        "enable", false
    );

/* # Form Related Plugins
================================================== */

	//===== Dual select boxes =====//
	
	// $.configureBoxes();

	/*$('#multiselect').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        },
        fireSearch: function(value) {
            return value.length > 3;
        }
    });*/

    //===== jQuery UI Datepicker =====//

	$( ".datepicker" ).datepicker({
		showOtherMonths: true,
		dateFormat: 'yy-mm-dd'
    });

    $( ".datepicker_after" ).datepicker({
    	showOtherMonths: true,
		maxDate: new Date(),
		changeMonth: true,
		yearRange: '1900:+0',
    	changeYear: true,
    	dateFormat: 'yy-mm-dd'
    });

    $( ".datepicker_before" ).datepicker({
    	showOtherMonths: true,
		minDate: new Date(),
		changeMonth: true,
		yearRange: '2010:2100',
    	changeYear: true,
    	dateFormat: 'yy-mm-dd'
    });

    $( ".datepicker-inline" ).datepicker({ showOtherMonths: true });

    $( ".datepicker-multiple" ).datepicker({
    	showOtherMonths: true,
      	numberOfMonths: 3
    });

    $( ".datepicker-trigger" ).datepicker({
      showOn: "button",
      buttonImage: "images/interface/datepicker_trigger.png",
      buttonImageOnly: true,
      showOtherMonths: true
    });

    $( ".from-date" ).datepicker({
      defaultDate: "+1w",
      numberOfMonths: 3,
      showOtherMonths: true,
      onClose: function( selectedDate ) {
        $( ".to-date" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( ".to-date" ).datepicker({
      defaultDate: "+1w",
      numberOfMonths: 3,
      showOtherMonths: true,
      onClose: function( selectedDate ) {
        $( ".from-date" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

    $( ".datepicker-restricted" ).datepicker({ minDate: -20, maxDate: "+1M +10D", showOtherMonths: true });

    //===== WYSIWYG editor =====//

	/*$('.editor').wysihtml5({
	    stylesheets: "css/wysihtml5/wysiwyg-color.css"
	});*/

});