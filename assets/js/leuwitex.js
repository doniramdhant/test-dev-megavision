                                    function format_number(value){
                                        $('#'+value).on("keydown keyup", function (event) {
                                            $('#'+value).val(format_rupiah(this.value));

                                            var clear_dot       = this.value.split('.').join("");
                                            var change_comma    = clear_dot.split(',').join(".");
                                            $('#'+value+'_hide').val(change_comma);
                                        });
                                    }

                                    function format_rupiah(angka, prefix)
                                    {
                                        var number_string   = angka.replace(/[^,\d]/g, '').toString(),
                                        split               = number_string.split(','),
                                        sisa                = split[0].length % 3,
                                        rupiah              = split[0].substr(0, sisa),
                                        ribuan              = split[0].substr(sisa).match(/\d{3}/gi);

                                        if(ribuan){
                                            separator = sisa ? '.' : '';
                                            rupiah += separator + ribuan.join('.');
                                        }

                                        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                                        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
                                    }

                                    function format_fokus(id) {
                                        $('#'+id).select();
                                    }

                                    function formatcemua(input){
                                        var num = input.toString();
                                        if(!isNaN(num)){
                                            if(num.indexOf('.') > -1){
                                                num = num.split('.');
                                                num[0] = num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.').split('').reverse().join('').replace(/^[\.]/,'');
                                                if(num[1].length > 2){
                                                    while(num[1].length > 2){
                                                        num[1] = num[1].substring(0,num[1].length-1);
                                                    }
                                                }
                                                input = num[0];  
                                            }else{
                                                input = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.').split('').reverse().join('').replace(/^[\.]/,'')
                                            };
                                        }
                                            var clear_dot       = input.split('.').join("");
                                            var change_comma    = clear_dot.split(',').join(".");
                                            $('#ntotalbersih_hide').val(change_comma);
                                            //$('#ntotal_hide').val(change_comma);
                                        return input;
                                    }
                                    
                                    function reformat(input){
                                            var num=input.value.replace(/\,/g,'');if(!isNaN(num)){if(num.indexOf('.')>-1){num=num.split('.');num[0]=num[0].toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');if(num[1].length>2){alert('maksimum 2 desimal !!!');num[1]=num[1].substring(0,num[1].length-1);}
                                            input.value=num[0]+'.'+num[1];}else{input.value=num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1,').split('').reverse().join('').replace(/^[\,]/,'');}}else{alert('input harus numerik !!!');input.value=input.value.substring(0,input.value.length-1);}}