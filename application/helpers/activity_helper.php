<?php

function activity_helper($tipe = "", $str = "", $ip=""){
    $CI =& get_instance();
 
    if (strtolower($tipe) == "login"){
        $log_tipe   = 0;
    }
    elseif(strtolower($tipe) == "logout")
    {
        $log_tipe   = 1;
    }
    elseif(strtolower($tipe) == "add"){
        $log_tipe   = 2;
    }
    elseif(strtolower($tipe) == "edit"){
        $log_tipe  = 3;
    }
    elseif(strtolower($tipe) == "delete"){
        $log_tipe  = 4;
    }
    elseif(strtolower($tipe) == "gagal"){
        $log_tipe  = 5;
    }
    elseif(strtolower($tipe) == "print"){
        $log_tipe  = 6;
    }
    else{
        $log_tipe  = 7;
    }
 
    // paramter
    $param['log_time']      = date('Y-m-d H:i:s');
    $param['log_user']      = $CI->session->userdata('user_name');
    $param['log_tipe']      = $log_tipe;
    $param['log_desc']      = $str;
    $param['log_ipaddress'] = $ip;
    

    //load model log
    $CI->load->model('logger');
 
    //save to database
    $CI->logger->save_log($param);

}
?>