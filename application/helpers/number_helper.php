<?php
	function cekdesimal( $value ) {
	    if ( strpos( $value, "." ) !== false ) {
	    	if ( ( substr( $value , -3 ) == ".00" ) !== false ) {
		        return false;
		    } else {
	        	return true;
	        }
	    }
	    return false;
	}

	function rupiah($number)
	{	
		return number_format($number, 0, ',', '.');
	}

	function rupiah_laporan($number)
	{	
		return number_format($number, 0, '.', ',');
	}

	function stok_integer($number)
	{	
		return number_format($number, 0, '.', ',');
	}

	function stok_double($number)
	{	
		return number_format($number, 2, '.', ',');
	}