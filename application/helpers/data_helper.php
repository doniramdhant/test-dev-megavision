<?php
function getDataPengaturanGlobal($param = null) {
    $CI = &get_instance();
    $CI->load->model('utility/Utility_model');
    $data = $CI->Utility_model->select('Pengaturan', '*', array('KodePengaturan' => $param))->row();

    if (empty($data)) {
        return null;
    } else {
        return $data->Nilai;
    }
}

?>