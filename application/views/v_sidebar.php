<style>
    a.link-sidebar:hover {
        border-left: 3px solid #0D8DFA;
    }

    .page-sidebar .page-sidebar-menu .sub-menu>li.active>a {
        border-left: 3px solid #0D8DFA;
    }

    .page-sidebar .page-sidebar-menu .sub-menu>li:hover>a,
    .page-sidebar .page-sidebar-menu .sub-menu>li.open>a {
        border-left: 3px solid #0D8DFA;
    }
</style>
      <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse">
                <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                 
                </ul>
                <!-- END SIDEBAR MENU -->
            </div>
        </div>
        <!-- END SIDEBAR -->