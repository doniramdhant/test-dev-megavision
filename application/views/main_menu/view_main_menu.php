<!DOCTYPE html>
<html lang="en" class="no-js">
    <?php $this->load->view('v_header') ?>
   <head>
    <meta charset="utf-8" />
    <title>MEGAVISION</title>
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/global/img/Logo.png" type="image/x-icon" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solidpage-sidebar-closed  pace-done page-sidebar-closed">
    <body class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo page-container-bg-solidpage-sidebar-closed  pace-done page-sidebar-closed">
    <script>
        
        $(function () {
            $('#container3').highcharts({
                chart: {
                        zoomType: 'xy'
                    },
                    title: {
                        text: 'Summary Faktur By Buyer Years'
                    },
                    subtitle: {
                        text: 'Source: kitabikin.com'
                    },
                    xAxis: [{
                        categories: [<?php foreach ($grafik as $value) { ?>
                                '<?php echo $value->month; ?>',
                        <?php } ?>],
                        crosshair: true
                    }],
                    yAxis: [{ // Primary yAxis
                        labels: {
                            format: '{value:,.0f}',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        },
                        title: {
                            text: 'Rp',
                            style: {
                                color: Highcharts.getOptions().colors[1]
                            }
                        }
                    }, { // Secondary yAxis
                        title: {
                            text: 'Total',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        labels: {
                            format: '{value:,.0f}',
                            style: {
                                color: Highcharts.getOptions().colors[0]
                            }
                        },
                        opposite: true
                    }],
                    tooltip: {
                        shared: true
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'left',
                        x: 120,
                        verticalAlign: 'top',
                        y: 100,
                        floating: true,
                        backgroundColor:
                            Highcharts.defaultOptions.legend.backgroundColor || // theme
                            'rgba(255,255,255,0.25)'
                    },
                    series: [{
                        name: 'Total Faktur',
                        type: 'column',
                        yAxis: 1,
                        data: [<?php foreach ($grafik as $value) { ?>
                                <?php echo $value->totalincome; ?>,
                        <?php } ?>],
                        tooltip: {
                            valueSuffix: '.'
                        }

                    }, {
                        name: 'SJ/Packing List',
                        type: 'spline',
                        data: [<?php foreach ($grafik as $value) { ?>
                                <?php echo $value->totalincome; ?>,
                        <?php } ?>],
                        tooltip: {
                            valueSuffix: '.'
                        }
                    }]
            });
        });

        </script>

        <?php $this->load->view('v_horizontal_menu') ?>

        <div class="clearfix">
        </div>

        <!-- BEGIN CONTAINER -->
        <div class="page-container">

            <?php $this->load->view('v_sidebar') ?>

            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="fa fa-home"></i>
                                <a href="<?php echo site_url('main_menu') ?>">Main Menu</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="#">Master</a>
                            </li>
                        </ul>
                        <div class="page-toolbar">
                            <div id="dashboard-report-range" class="pull-right tooltips btn btn-fit-height grey-salt" data-container="body" data-placement="bottom" data-original-title="Kalender">
                                <i class="icon-calendar"></i>&nbsp; 
                                <span class="thin uppercase visible-lg-inline-block">&nbsp;</span>&nbsp; 
                                <i class="fa fa-angle-down"></i>
                            </div>
                        </div>
                    </div>
                    <h3 class="page-title">
                        Dashboard</small>
                    </h3>
                    <!-- END PAGE HEADER-->
                   <div class="row clearfix">
                        <div class="col-sm-3 column">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Sales 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="tx-card" class="card-body">
                                        <p align="center">
                                            <button class="btn button btn-info" type="button" id="button">Refresh</button>
                                        </p>
                                        <form action="#">
                                            <div class="wmin-sm-200">
                                                <!--select id="dashperson" class="form-control form-control-select2" 
                                                    onchange="doRefresh();">
                                                    <option value="HI">Hari ini 2</option>
                                                    <option value="HI" selected>Pilih</option>
                                                    <!--option value="7HT">7 hari terakhir</option>
                                                    <option value="BI">Bulan ini</option>
                                                    <option value="BL">Bulan lalu</option>-->

                                                <!--/select>-->
                                            </div>
                                        </form>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3">
                                            <div class="card card-body">
                                                    <div class="media">
                                                        <div class="media-body text-center">
                                                            <i class="fa fa-archive fa-5x"></i>
                                                            <h5 class="font-weight-semibold mb-0">
                                                                <h2><span id="total-sales"></span></h2>
                                                            </h5>
                                                            <span class="text-uppercase font-size-sm text-muted"><h3>Total Sales</h3></span>
                                                            <br>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> 
                        <div class="col-sm-3 column">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Income 
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="tx-card1" class="card-body">
                                        <p align="center">
                                            <button class="btn button btn-info" type="button" id="button">Refresh</button>
                                        </p>
                                        <form action="#">
                                            <div class="wmin-sm-200">
                                                <!--select id="dashperson" class="form-control form-control-select2" 
                                                    onchange="doRefresh();">
                                                    <option value="HI">Hari ini 2</option>
                                                    <option value="HI" selected>Pilih</option>
                                                    <!--option value="7HT">7 hari terakhir</option>
                                                    <option value="BI">Bulan ini</option>
                                                    <option value="BL">Bulan lalu</option>-->

                                                <!--/select>-->
                                            </div>
                                        </form>
                                        <br>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-3">
                                            <div class="card card-body">
                                                    <div class="media">
                                                        <div class="media-body text-center">
                                                            <i class="fa fa-dollar fa-5x"></i>
                                                            <h5 class="font-weight-semibold mb-0">
                                                                <h2>Rp.<span id="total-income"></span></h2>
                                                            </h5>
                                                            <span class="text-uppercase font-size-sm text-muted"><h3>Total Income</h3></span>
                                                            <br>
                                                            <br>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="row clearfix">
                    <div class="col-md-12 column">
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Sales Performance
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                
                                    <div class="table-scrollable">
                                    <div id="container3" style="min-width: 310px; height: 500px; max-width: 1024px; margin: 0 auto"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
  
                    </div>
                    <div class="row clearfix">
                        <div class="col-md-12 column">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">
                                        Order
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse">
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                        <h4 align="center"><b>Recent Order</b></h4><br>
                                    <div class="table-scrollable">
                                        <div>
                                            <div class="table-responsive">
                                    <table class="display nowrap" id="dataTable">
                                        <thead>
                                            <tr>
                                                <th>
                                                    Name
                                                </th>
                                                <th>
                                                    Order Date
                                                </th>
                                                <th>
                                                    Order Amount
                                                </th>
                                                <th>
                                                    Phone
                                                </th>
                                                <th>
                                                    Sales
                                                </th>
                                                <th>
                                                    Office
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                        </tbody>
                                    </table>
                                </div>  
                                <script type="text/javascript">
                                        $(document).ready(function() {
                                            $('#dataTable thead th').each(function () {
                                                var title = $(this).text();
                                                $(this).html(title+' <br><input type="text" class="col-search-input" placeholder="Search ' + title + '" />');
                                            });
                                            var table = $('#dataTable').DataTable({
                                                            "ajax": {
                                                            url : "<?php echo site_url("main_menu/recentorder") ?>",
                                                            type : 'GET',
                                                            error: function(data, err){ 
                                                                alert("Tidak ditemukan data yang sesuai.");
                                                                $(".dataTable-error").html("");
                                                                $("#dataTable").append('<tr><td class="text-center" colspan="'+column+'">Tidak ditemukan data yang sesuai.</td></tr>');
                                                                $("#dataTable_processing").css("display","none");
                                                            },
                                                        },
                                                            "dom": 'lBfrtip',
                                                            buttons: [
                                                                    {
                                                                        extend: 'excel',
                                                                        text: '<span class="fa fa-file-excel-o"></span> Excel Export',
                                                                        exportOptions: {
                                                                            modifier: {
                                                                                search: 'applied',
                                                                                order: 'applied'
                                                                            }
                                                                        }
                                                                    }
                                                                ],
                                                                columnDefs: [{
                                                                    targets: "_all",
                                                                    orderable: false
                                                                }],
                                                                lengthMenu: [[10, 50, 100, -1], [10, 50, 100, "All"]],
                                                                pageLength: 6,
                                                    });
                                                    table.columns().every(function () {
                                                        var table = this;
                                                        $('input', this.header()).on('keyup change', function () {
                                                            if (table.search() !== this.value) {
                                                                table.search(this.value).draw();
                                                            }
                                                        });
                                                    });
                                        });
                                    </script>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    
                    <script type="text/javascript">
                        $.ajax({
                                cache: false,
                                dataType: 'json',
                                type: 'get',
                                url: 'get_dashsales',
                                data: {
                                },
                                success:function(data)
                                {
                                    console.log(data['totalsales']);
                                    var total_sales = 0;
                                    //var table = '';

//                                    $.each(data, function(k, v) {

                                        parse_sales = parseInt(data['totalsales']);
                                        total_sales = total_sales + parse_sales;
                                    //});

                                    $('#total-sales').html(total_sales.toLocaleString('en-US'));
                                    $('#tx-card').unblock();
                                }
                            });
                        

                        function doRefresh() {
                            $.ajax({
                                cache: false,
                                dataType: 'json',
                                type: 'get',
                                url: 'get_dashsales',
                                data: {
                                },
                                beforeSend: function() 
                                {
                                    $('#tx-card').block({ 
                                        message: '<i class="icon-spinner2 spinner"></i>',
                                        overlayCSS: {
                                            backgroundColor: '#fff',
                                            opacity: 0.8,
                                            cursor: 'wait',
                                            'box-shadow': '0 0 0 1px #ddd'
                                        },
                                        css: {
                                            border: 0,
                                            padding: 0,
                                            backgroundColor: 'none'
                                        }
                                    });
                                },
                                success:function(data)
                                {
                                    //console.log(data);
                                    var total_sales = 0;
                                    //var table = '';

                                    //$.each(data, function(k, v) {

                                        parse_sales = parseInt(data['totalsales']);
                                        total_sales = total_sales + parse_sales;
                                   // });
                                    $('#tx-card').unblock();
                                }
                            });
                        }

                        $.ajax({
                                cache: false,
                                dataType: 'json',
                                type: 'get',
                                url: 'get_dashincome',
                                data: {
                                },
                                success:function(data)
                                {
                                    //console.log(data);
                                    var total_income = 0;
                                    //var table = '';

                                 //   $.each(data, function(k, v) {

                                        parse_income = parseInt(data['totalincome']);
                                        total_income = total_income + parse_income;
                                   // });

                                    $('#total-income').html(total_income.toLocaleString('en-US'));
                                    $('#tx-card').unblock();
                                }
                            });
                        

                        function doRefresh2() {
                            $.ajax({
                                cache: false,
                                dataType: 'json',
                                type: 'get',
                                url: 'get_dashincome',
                                data: {
                                },
                                beforeSend: function() 
                                {
                                    $('#tx-card1').block({ 
                                        message: '<i class="icon-spinner2 spinner"></i>',
                                        overlayCSS: {
                                            backgroundColor: '#fff',
                                            opacity: 0.8,
                                            cursor: 'wait',
                                            'box-shadow': '0 0 0 1px #ddd'
                                        },
                                        css: {
                                            border: 0,
                                            padding: 0,
                                            backgroundColor: 'none'
                                        }
                                    });
                                },
                                success:function(data)
                                {
                                    //console.log(data);
                                    var total_income = 0;
                                    //var table = '';

                                   // $.each(data, function(k, v) {

                                        parse_income = parseInt(data['totalincome']);
                                        total_income = total_income + parse_income;
                                    //});

                                    $('#tx-card1').unblock();
                                }
                            });
                        }
                    /*$(function () {
                        setInterval(doRefresh, 60000);
                    });*/

                    $(window).load(function() {      
                      $("#button").click(function() {
                            doRefresh();
                            doRefresh2();
                        $("#div").load(" #div > *");
                      });
                    });


                    $(document).ready(function() {
                        $('.input-group.date').datepicker({
                            format: "dd-mm-yyyy",
                            autoclose:true,
                            todayHighlight: true,
                            orientation: "left top",
                            toggleActive: true
                        }).attr("readonly", "readonly").css({"cursor":"pointer", "background":"white"});
                    });
            </script>
                </div>
            </div>
        </div>
        <!-- END CONTAINER -->
        <?php $this->load->view('v_footer') ?>
    </body>
    <!-- END BODY -->
</body>
</html>
