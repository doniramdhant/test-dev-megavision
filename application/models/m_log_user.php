<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_log_user extends CI_Model {

    public function get_log_user() {
        $this->db->order_by('ID', 'asc');
        $query = $this->db->get("LOG_USERS");
        return $query;
    }

    public function hapus_log_user($id) {
        $this->db->where('ID', $id);
        $this->db->delete('LOG_USERS', array('ID' => $id));
    }

}
