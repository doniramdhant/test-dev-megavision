<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class m_main_menu extends CI_Model {

    public function get_recent_order()
    {
      $query = $this->db->query("select * from v_recentorder");
      if ($query->num_rows() > 0) {
        return $query;
      }
    }

    public function get_grafik()
    {
      $query = $this->db->get('v_dashgr');
      return $query->result();
    }
  
}
