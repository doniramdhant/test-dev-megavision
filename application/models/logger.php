<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Logger extends CI_Model{
	public function __construct()
	{
		parent::__construct();
	}

	public function save_log($param)
    {
        $sql        = $this->db->insert_string('log',$param);
        $ex         = $this->db->query($sql);
        return $this->db->affected_rows($sql);
    }

	function write($user_id, $ip_address, $waktu, $message) {
		$ip_address = $this->input->ip_address();
		if($user_id!='') { 
			$data['user_id'] 	= $user_id;
			$data['ip_address']= $ip_address;
			$data['waktu'] = $waktu;
			$data['activity'] 	= $message;
			$this->db->insert('megavision_log',$data);
		}
	} 
	
	function session_item($session_id, $user_id, $waktu, $area){
		if($user_id!='') { 
			$data['id'] 	= $session_id;
			$data['user_id']= $user_id;
			$data['waktu'] = $waktu;
			$data['area'] = $area;
			$this->db->insert('session_item',$data);
		}
	} 
}
?>
