<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class M_user extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    function cek_user() {
        $user_name = $this->input->post('name');
        $pass = md5($this->input->post('password'));
        $this->db->where('name', $user_name);
        $this->db->where('password', $pass);
        $query = $this->db->get('tr_user_global');

        if ($query->num_rows() == 0) {
            $this->session->set_flashdata('result_login', "Maaf user name atau password salah");
        }

        return $query->result();
    }

}

?>