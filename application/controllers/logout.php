<?php

class Logout extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
    	$sess=$this->session->userdata('session_id');
		$id=$this->session->userdata('user_id');
		$sql	= "select * from session where session_id='$sess' ";
		$rs		= pg_query($sql);
		if(pg_num_rows($rs)>0){
			while($row=pg_fetch_assoc($rs)){
				$ip_address	  = $row['ip_address'];
				break;
			}
		}else{
			$ip_address='kosong';
		}
		$query 	= pg_query("SELECT current_timestamp as c");
    	while($row=pg_fetch_assoc($query)){
    	$now	  = $row['c'];
		}
        $this->session->sess_destroy();  //Hapus seluruh session
        $this->session->sess_create();   // Buat kembali session karena kita akan mengirim flashdata ke halaman login
        $this->session->set_flashdata('message_logout', TRUE);
        redirect('login');
    }

}
