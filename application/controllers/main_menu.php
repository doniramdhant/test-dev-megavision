<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Main_menu extends CI_Controller {

//    var $client     = LinkBerita;

    public function __construct() {
        parent::__construct();
        /*if (production) {
            //$this->url      = "http://192.168.86.57:8200";
            $this->url      = "http://192.168.0.21:8200";
            $this->token    = "Token fec95bf37c16d481eb6930519f63d0591a6c2c39";
        } else {
            $this->url      = "http://192.168.255.240:8400";
            $this->token    = "Token 0797e6a8f266dcd20e9a5e2237a6a2322253e438";
        }
*/
        $this->load->library('curl');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('ApiCurl');
        $this->load->library('session');
 //       $this->api_key  = APIBerita;
        $this->load->model('m_user');
        $this->load->model('m_main_menu');
        $this->load->model('m_asset');
        session_start();
        if ($this->session->userdata('group_id') == null) {
            $this->session->set_flashdata('session_expired', TRUE);
            redirect('login');
        }
    }

    public function index() {
        if ($this->session->userdata('group_id') == '1') {
            $this->su();
        } else if ($this->session->userdata('group_id') == '2') {
            $this->admin();
        }
    }

    public function su() {
        if ($this->session->userdata('group_id') != '1') {
            redirect('login');
        }
        $data['main_menu'] = "Main Menu Super User";
        $this->load->view('main_menu/view_main_menu', $data);
    }

    public function admin() {
        if ($this->session->userdata('group_id') != '2') {
            redirect('login');
        }
        $data['main_menu'] = "Main Menu Administrator"; 
        $riw = $this->m_main_menu->get_grafik();
        $data['grafik'] = $riw;
        $this->load->view('main_menu/view_main_menu', $data);
    }

    public function admin_mobile() {
        if ($this->session->userdata('group_id') != '2') {
            redirect('login');
        }
        $data['main_menu'] = "Main Menu Administrator"; 
     
        //var_dump($array[]=$news->articles[$i]->title);
        //die();
        $this->load->view('main_menu/view_main_menu_mobile', $data);
    }

    public function get_dashsales()
    {

            $querys  = $this->db->query("select * from v_dashsales");
            $riw    = $querys->row();

            //var_dump($data[0]);
            if ($riw) {
           
                echo json_encode($riw);
              
            } else {
                $arr_result[] = "Tidak ada data";
                echo json_encode($arr_result);
            }
        
    }

    public function get_dashincome()
    {

            $querys  = $this->db->query("select * from v_dashincome");
            $riw    = $querys->row();

            //var_dump($data[0]);
            if ($riw) {
           
                echo json_encode($riw);
              
            } else {
                $arr_result[] = "Tidak ada data";
                echo json_encode($arr_result);
            }
        
    }

    public function get_dashgrafik()
    {

        

            $riw = $this->m_main_menu->get_grafik();
            
            if ($riw) {
           
                echo json_encode($riw);
              
            } else {
                $arr_result[] = "Tidak ada data";
                echo json_encode($arr_result);
            }
        
    }

    public function recentorder()
    {

        $draw   = intval($this->input->get("draw"));
        $start  = intval($this->input->get("start"));
        $length = intval($this->input->get("length"));

        $ro = $this->m_main_menu->get_recent_order();

        $data = array();
        $no = $start + 1;
        foreach ($ro->result() as $r) {
            
                $data[] = array(
                    $r->client_name,
                    $r->order_days.', '.$r->order_date,
                    $r->order_amount,
                    $r->client_phone,
                    $r->employee_name,
                    $r->e_office
                );
            $no++;
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => $ro->num_rows(),
            "recordsFiltered" => $ro->num_rows(),
            "data" => $data
        );

        echo json_encode($output);
        exit();
    }


}

?>
