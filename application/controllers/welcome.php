<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->model('m_sdm_luasdaerah');
        $this->load->model('m_keahlian_maritim');
        $this->load->model('m_kota');
        $this->load->model('m_provinsi');
        $this->load->model('m_sdm_kekuatanpns');
        $this->load->model('m_satuan');
        $this->load->model('m_sdm_data');
        $this->load->model('m_sdm_organisasi');
        $this->load->model('m_sdm_medis');
        $this->load->model('m_sdm_tenagakomcad');
        $this->load->model('m_keahlian_maritim');
        $this->load->model('m_sdm_purnawirawan');
        $this->load->model('m_odirga');
        $this->load->model('m_prasaka');
    }

    public function index() {
        $this->load->view('welcome_message');
    }

    function coba() {
        $filename = "Fatkur_Penjualan" . date('d-m-Y');
        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        //ini_set('memory_limit', '20M');
        $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_wilayah');

        $pdf = $this->pdf->load();
        $pdf->AddPage('P');
        $pdf->SetFooter($filename . '|{PAGENO}|' . date(DATE_RFC822));
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, 'F');
    }

//    public function cetak_wilayah() {
//        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
//        ini_set("memory_limit", "64M");
//        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
//        $hm = $h * 60;
//        $ms = $hm * 60;
//        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));
//
//        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
//
//        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
//        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
//        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
//        $data['tanggal'] = $this->input->post('tanggal');
//        $data['nama'] = $this->input->post('nama');
//        $data['pangkat'] = $this->input->post('pangkat');
//        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
//
//        if (file_exists($pdfFilePath) == FALSE) {
//            $id_satuan = '0';
//            if (isset($_GET['id_satuan'])) {
//                if ($_GET['id_satuan'] != '') {
//                    $id_satuan = $_GET['id_satuan'];
//                }
//            }
//            $data['satuan_id'] = $id_satuan;
//            $data['satuan'] = $this->m_satuan->get_satuan();
//            $data['rekap_sdm'] = $this->m_sdm_data->sdm_dalam_penerbangan_rekapitulasi($id_satuan);
//            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_wilayah', $data, true); // render the view into HTML
//            $this->load->library('pdf');
////            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
////            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
//            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
//            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
//            $pdf->WriteHTML($html); // write the HTML into the PDF
//            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
//        }
//
////        redirect("assets/pdf/" . $filename . ".pdf");
//    }

    public function cetak_wilayah_terperinci() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Data_SDM_Terperinci" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/$filename.pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan_terperinci'])) {
                if ($_GET['id_satuan_terperinci'] != '') {
                    $id_satuan = $_GET['id_satuan_terperinci'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['terperinci_sdm'] = $this->m_sdm_data->sdm_dalam_penerbangan_terperinci($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_wilayah_terperinci', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

        redirect("assets/pdf/$filename.pdf");
    }

    public function cetak_wilayah() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";

        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan'])) {
                if ($_GET['id_satuan'] != '') {
                    $id_satuan = $_GET['id_satuan'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['satuan'] = $this->m_satuan->get_satuan();
            $data['rekap_sdm'] = $this->m_sdm_data->sdm_dalam_penerbangan_rekapitulasi($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_wilayah', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_laporan_bid_sdm() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";

        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan'])) {
                if ($_GET['id_satuan'] != '') {
                    $id_satuan = $_GET['id_satuan'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['satuan'] = $this->m_satuan->get_satuan();
            if ($id_satuan != 0) {
                $data['rekap_sdm'] = $this->m_sdm_data->sdm_rekapitulasi($id_satuan);
            } else if ($id_satuan == 0) {
                $data['rekap_sdm'] = $this->m_sdm_data->sdm_rekapitulasi_all();
            }
            $html = $this->load->view('sdm_cetak/view_cetak_bid_sdm', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

//    public function cetak_purnawirawan() {
//        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
//        ini_set("memory_limit", "64M");
//        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
//        $hm = $h * 60;
//        $ms = $hm * 60;
//        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));
//
//        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
//
//        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
//        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
//        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
//        $data['tanggal'] = $this->input->post('tanggal');
//        $data['nama'] = $this->input->post('nama');
//        $data['pangkat'] = $this->input->post('pangkat');
//        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
//
//        if (file_exists($pdfFilePath) == FALSE) {
//            $id_satuan = '0';
//            if (isset($_GET['id_satuan'])) {
//                if ($_GET['id_satuan'] != '') {
//                    $id_satuan = $_GET['id_satuan'];
//                }
//            }
//            $data['satuan_id'] = $id_satuan;
//            $data['satuan'] = $this->m_satuan->get_satuan();
//            $data['rekap_purnawirawan'] = $this->m_sdm_purnawirawan->sdm_purnawirawan_rekapitulasi($id_satuan);
//            $html = $this->load->view('sdm_cetak/view_cetak_purnawirawan', $data, true); // render the view into HTML
//            $this->load->library('pdf');
////            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
////            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
//            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
//            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
//            $pdf->WriteHTML($html); // write the HTML into the PDF
//            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
//        }
//
////        redirect("assets/pdf/" . $filename . ".pdf");
//    }

    public function cetak_purnawirawan() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";

        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan'])) {
                if ($_GET['id_satuan'] != '') {
                    $id_satuan = $_GET['id_satuan'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['satuan'] = $this->m_satuan->get_satuan();
            $data['rekap_purnawirawan'] = $this->m_sdm_purnawirawan->sdm_purnawirawan_rekapitulasi($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_purnawirawan', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt",""10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
//            $pdf->AddPage('L'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_purnawirawan_terperinci() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Data_Purnawirawan_Terperinci" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/$filename.pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
//        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan_terperinci'])) {
                if ($_GET['id_satuan_terperinci'] != '') {
                    $id_satuan = $_GET['id_satuan_terperinci'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['pangkat1'] = $this->m_sdm_purnawirawan->get_pangkat();
            $data['terperinci_purnawirawan'] = $this->m_sdm_purnawirawan->sdm_purnawirawan_terperinci($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_purnawirawan_terperinci', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

        redirect("assets/pdf/$filename.pdf");
    }

    public function cetak_wilayah_download() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_wilayah', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'D'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_instansi() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_SDM_Dunia_Penerbangan_Per_Lanud_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_instansi', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_instansi_download() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_SDM_Dunia_Penerbangan_Per_Lanud_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_instansi', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'D'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_masyarakat() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_masyarakat', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_masyarakat_download() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_SDM_Dunia_Penerbangan_Per_Masyarakat_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_dunia_penerbangan_masyarakat', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'D'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_penduduk() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_Menurut_Umur_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        $data['data_jml_umur'] = $this->m_sdm_luasdaerah->get_penduduk_umur();
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_data_penduduk_umur', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_penduduk_download() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_Menurut_Umur_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['data_jml_umur'] = $this->m_sdm_luasdaerah->get_penduduk_umur();
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_data_penduduk_umur', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'D'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_penduduk_kbm() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_KBM_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['laporan_kbm'] = $this->m_keahlian_maritim->get_laporan_kbm();
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_data_penduduk_kbm', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_penduduk_kbm_download() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_KBM_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['laporan_kbm'] = $this->m_keahlian_maritim->get_laporan_kbm();
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_data_penduduk_kbm', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'D'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak2() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/$filename.pdf";
        //$data['query'] = $this->ter_model->w1();
        //$data['nama_resor'] = $_GET['nama_resor']; //$this->input->post('nama_resor');
        $data['nama_distrik'] = "";

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('view_cetak_sdm_kualifikasi_khusus', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

        redirect("assets/pdf/$filename.pdf");
    }

    public function cetak_pns_kemhan() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Kekuatan_pns_kemhan_tni_polri_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        $data['jml_pns_kemhan'] = $this->m_sdm_kekuatanpns->laporan_pns_kemhan();
        $data['provinsi'] = $this->m_provinsi->get_provinsi();
        $data['kota'] = $this->m_kota->get_kota();
        $data['satuan'] = $this->m_satuan->get_satuan();

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_kekuatan_pns_kemhan_tni_polri', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_pns_kemhan_download() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Kekuatan_PNS_KEMHAN_TNI_POLRI__" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        $data['jml_pns_kemhan'] = $this->m_sdm_kekuatanpns->laporan_pns_kemhan();
        $data['provinsi'] = $this->m_provinsi->get_provinsi();
        $data['kota'] = $this->m_kota->get_kota();
        $data['satuan'] = $this->m_satuan->get_satuan();

        if (file_exists($pdfFilePath) == FALSE) {
            $html = $this->load->view('sdm_cetak/view_cetak_kekuatan_pns_kemhan_tni_polri', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'D'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_sdm() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_Menurut_Umur_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));


        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $data['umum'] = $this->m_sdm_data->get_sdm_umum();
            $data['tni'] = $this->m_sdm_data->get_sdm_tni();
            $data['sdm_data'] = $this->m_sdm_data->get_sdm_data();
            $data['provinsi'] = $this->m_sdm_data->get_provinsi();
            $data['satuan'] = $this->m_satuan->get_satuan();
            $data['profesi'] = $this->m_sdm_data->get_profesi_2();
            $html = $this->load->view('sdm_cetak/view_cetak_data_sdm', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_organisasi() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_Menurut_Umur_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));


        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $data['organisasi'] = $this->m_sdm_organisasi->get_organisasi();
            $html = $this->load->view('laporan_organisasi/cetak_lap_organisasi', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_medis() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_Menurut_Umur_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));


        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $data['m_sdm_medis'] = $this->m_sdm_medis->get_sdm_medis();
            $data['provinsi'] = $this->m_sdm_tenagakomcad->get_provinsi();
            $data['kota'] = $this->m_sdm_medis->get_kota();
            $html = $this->load->view('laporan_medis/cetak_lap_medis', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_komcad() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Penduduk_Menurut_Umur_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));


        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */
        $data['kota'] = $this->m_kota->get_kota();

        if (file_exists($pdfFilePath) == FALSE) {
            $data['provinsi'] = $this->m_sdm_tenagakomcad->get_provinsi();
            $data['satuan'] = $this->m_sdm_tenagakomcad->get_satuan();
            $data['m_sdm_tenagakomcad'] = $this->m_sdm_tenagakomcad->get_sdm_tenagakomcad();
            $html = $this->load->view('laporan_komcad/cetak_lap_komcad', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

//    public function cetak_odirga() {
//        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
//        ini_set("memory_limit", "64M");
//        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
//        $hm = $h * 60;
//        $ms = $hm * 60;
//        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));
//
//        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";
//
//        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
//        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
//        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
//        $data['tanggal'] = $this->input->post('tanggal');
//        $data['nama'] = $this->input->post('nama');
//        $data['pangkat'] = $this->input->post('pangkat');
//        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
//
//        if (file_exists($pdfFilePath) == FALSE) {
//            $id_satuan = '0';
//            if (isset($_GET['id_satuan'])) {
//                if ($_GET['id_satuan'] != '') {
//                    $id_satuan = $_GET['id_satuan'];
//                }
//            }
//            $data['satuan_id'] = $id_satuan;
//            $data['satuan'] = $this->m_satuan->get_satuan();
//            $data['rekap_odirga'] = $this->m_odirga->sdm_odirga_rekapitulasi($id_satuan);
//            $html = $this->load->view('sdm_cetak/view_cetak_odirga', $data, true); // render the view into HTML
//            $this->load->library('pdf');
////            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
////            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
//            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
//            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
//            $pdf->AddPage('L'); // Adds a new page in Landscape orientation
//            $pdf->WriteHTML($html); // write the HTML into the PDF
//            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
//        }
//    }

    public function cetak_odirga() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";

        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan'])) {
                if ($_GET['id_satuan'] != '') {
                    $id_satuan = $_GET['id_satuan'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['satuan'] = $this->m_satuan->get_satuan();
            $data['rekap_odirga'] = $this->m_odirga->sdm_odirga_rekapitulasi($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_odirga', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
//            $pdf->AddPage('L'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_odirga_all() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";

        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {

            $data['satuan'] = $this->m_satuan->get_satuan();
            $data['rekap_odirga'] = $this->m_odirga->sdm_odirga_rekapitulasi_all();
            $html = $this->load->view('sdm_cetak/view_cetak_odirga', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
//            $pdf->AddPage('L'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_odirga_terperinci() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Data_Purnawirawan_Terperinci" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/$filename.pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
//        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan_terperinci'])) {
                if ($_GET['id_satuan_terperinci'] != '') {
                    $id_satuan = $_GET['id_satuan_terperinci'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['spesialisasi'] = $this->m_odirga->get_spesialisasi();
            $data['terperinci_odirga'] = $this->m_odirga->sdm_odirga_terperinci($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_odirga_terperinci', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

        redirect("assets/pdf/$filename.pdf");
    }

    public function cetak_odirga_terperinci_all() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Data_Purnawirawan_Terperinci" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/$filename.pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
//        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */

        if (file_exists($pdfFilePath) == FALSE) {
            $data['spesialisasi'] = $this->m_odirga->get_spesialisasi();
            $data['terperinci_odirga'] = $this->m_odirga->sdm_odirga_terperinci_all();
            $html = $this->load->view('sdm_cetak/view_cetak_odirga_terperinci', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

        redirect("assets/pdf/$filename.pdf");
    }

    public function cetak_prasaka() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";

        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan'])) {
                if ($_GET['id_satuan'] != '') {
                    $id_satuan = $_GET['id_satuan'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['satuan'] = $this->m_satuan->get_satuan();
            $data['rekap_prasaka'] = $this->m_prasaka->sdm_prasaka_rekapitulasi($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_prasaka', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
//            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
//            $pdf->AddPage('L'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_prasaka_terperinci() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Data_Prasaka_Terperinci" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/$filename.pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan_terperinci'])) {
                if ($_GET['id_satuan_terperinci'] != '') {
                    $id_satuan = $_GET['id_satuan_terperinci'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['terperinci_prasaka'] = $this->m_prasaka->sdm_prasaka_terperinci($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_prasaka_terperinci', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

        redirect("assets/pdf/$filename.pdf");
    }

    public function cetak_sdm_organisasi() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Potensi_pertahanan_negara_" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/" . $filename . ".pdf";

        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['tandatangan'] = $this->input->post('tandatangan');
        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan'])) {
                if ($_GET['id_satuan'] != '') {
                    $id_satuan = $_GET['id_satuan'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['satuan'] = $this->m_satuan->get_satuan();
            $data['rekap_organisasi'] = $this->m_sdm_organisasi->laporan_organisasi($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_organisasi', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,6,8,5,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->AddPage('L'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

//        redirect("assets/pdf/" . $filename . ".pdf");
    }

    public function cetak_sdm_organisasi_terperinci() {
        // As PDF creation takes a bit of memory, we're saving the created file in /downloads/reports/
        ini_set("memory_limit", "64M");
        $h = "7"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
        $hm = $h * 60;
        $ms = $hm * 60;
        $filename = "Laporan_Data_Organisasi_Terperinci" . gmdate("D, d M Y H:i:s", time() + ($ms));

        $pdfFilePath = FCPATH . "assets/pdf/$filename.pdf";
        $data['judul_header1'] = strtoupper($this->input->post('judul_header1'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_header2'] = strtoupper($this->input->post('judul_header2'));
        $data['judul_laporan'] = strtoupper($this->input->post('judul_laporan'));
        $data['lampiran'] = $this->input->post('lampiran');
        $data['nomor'] = $this->input->post('nomor');
        $data['tandatangan'] = $this->input->post('tandatangan');
//        $data['sub_tandatangan'] = $this->input->post('sub_tandatangan');
//        $data['tempat'] = $this->input->post('tempat');
        $data['tanggal'] = $this->input->post('tanggal');
        $data['nama'] = $this->input->post('nama');
        $data['pangkat'] = $this->input->post('pangkat');
        /* $data['provinsi'] = $this->m_provinsi->get_provinsi(); */

        if (file_exists($pdfFilePath) == FALSE) {
            $id_satuan = '0';
            if (isset($_GET['id_satuan_terperinci'])) {
                if ($_GET['id_satuan_terperinci'] != '') {
                    $id_satuan = $_GET['id_satuan_terperinci'];
                }
            }
            $data['satuan_id'] = $id_satuan;
            $data['terperinci_organisasi'] = $this->m_sdm_organisasi->laporan_organisasi_terperinci($id_satuan);
            $html = $this->load->view('sdm_cetak/view_cetak_organisasi_terperinci', $data, true); // render the view into HTML
            $this->load->library('pdf');
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3'; // Landscape
//            $param = '"en-GB-x","Legal","","",10,10,10,10,6,3,array(190,236)'; // Landscape 190mm wide x 236mm height 
            $param = '"en-GB-x","A4","12pt","",10,10,10,10,6,3'; // Landscape 190mm wide x 236mm height 
            $pdf = $this->pdf->load($param);
            //$pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . gmdate("D, d M Y H:i", time() + ($ms))); // Add a footer for good measure
            $pdf->SetFooter();
//            $pdf->AddPage('P'); // Adds a new page in Landscape orientation
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $pdf->Output($pdfFilePath, 'I'); // save to file because we can
        }

        redirect("assets/pdf/$filename.pdf");
    }

}

/* End of file welcome.php */
    /* Location: ./application/controllers/welcome.php */    