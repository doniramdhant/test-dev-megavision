<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Under extends CI_Controller {
    
    var $link_api     = 'http://newsapi.org/v2/';

    public function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        $this->load->library('Curl');
        
        $this->id_      = 'id';
        $this->api_key  = '30f6b12aaa5b47e0965b927268e2ffa9';
//        session_start();
        if ($this->session->userdata('group_id') == '1') {
            redirect('main_menu/su');
        } else if ($this->session->userdata('group_id') == '2') {
            redirect('main_menu/admin');
        } else if ($this->session->userdata('group_id') == '3') {
            redirect('main_menu/purchase');
        } else if ($this->session->userdata('group_id') == '4') {
            redirect('main_menu/marketing');
        } else if ($this->session->userdata('group_id') == '5') {
            redirect('main_menu/akunting');
        } else if ($this->session->userdata('group_id') == '6') {
            redirect('main_menu/finance');
        } else if ($this->session->userdata('group_id') == '7') {
            redirect('main_menu/beacukai');
        } else if ($this->session->userdata('group_id') == '8') {
            redirect('main_menu/euclid');
        } else {
            
        }
    }

    public function index() {
        $data ['under'] = "Under Maintance";
        $this->load->view('maintenance/view_maintenance', $data);
    }

}

?>