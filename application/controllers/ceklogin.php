<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ceklogin extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
        /*if (production) {
            //$this->url      = "http://192.168.86.57:8200";
            $this->url      = "http://192.168.0.21:8200";
            $this->token    = "Token fec95bf37c16d481eb6930519f63d0591a6c2c39";
        } else {
            $this->url      = "http://192.168.255.240:8400";
            $this->token    = "Token 0797e6a8f266dcd20e9a5e2237a6a2322253e438";
        }*/

        $this->load->model('m_user');
        $this->load->library('session');
        $this->load->library('curl');
        $this->load->model('m_asset');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('ApiCurl');
        session_start();
        
    }

    function user() {
        
        $username = $this->input->post('username', TRUE);
        $password = $this->input->post('password', TRUE);

        if (isset($username) && $username != '')
        {
            if ($this->agent->is_mobile())
            {
            
                $user_id = $this->megavision->login_routine_mobile();
            
            }else if($this->agent->is_browser()){
               
                $user_id = $this->megavision->login_routine();

            }
        }
        else
        {
            if ($this->agent->is_browser())
            {
                $agent = $this->agent->browser().' '.$this->agent->version();
            }
            $data['page_title'] = $this->lang->line('login_login');
            $data['agen']=$agent;
            redirect('login');
        }
    }


}
