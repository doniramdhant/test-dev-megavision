<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {
    
  //  var $link_api     = 'LinkBerita';

    public function __construct() {
        parent::__construct();
        
        $this->load->library('session');
        $this->load->library('Curl');
        

        if ($this->session->userdata('group_id') == '1') {
            redirect('main_menu/su');
        } else if ($this->session->userdata('group_id') == '2') {
            redirect('main_menu/admin');
        } else {
            
        }
    }

    public function index() {
        $data ['login'] = "Login";
        $this->load->view('login/view_login', $data);
    }

}

?>
