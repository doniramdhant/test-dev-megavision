<?php
class ApiCurl {
    protected $CI;

    public function __construct()
    {
        $this->CI =& get_instance();

        $this->CI->load->library('session', 'input');
        if (production) {
            $this->token = 'fec95bf37c16d481eb6930519f63d0591a6c2c39';
        } else {
            $this->token = 'fec95bf37c16d481eb6930519f63d0591a6c2c39';
        }
    }
    public function GetCurl($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type:application/json',
                    'Authorization: Token '.$this->token
                ));    
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function PostCurl($url ,$data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));           
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/x-www-form-urlencoded',
            'Authorization: Token '.$this->token
        ));
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($ch);

        return $result;
    }

    public function UpdateCurl($url ,$data) {
        $ch = curl_init();
        $headers  = [
                    'Content-Type:application/x-www-form-urlencoded',
                    'Authorization: Token '.$this->token
                ];
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));           
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($ch);

        return $result;
    }

    public function DeleteCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type:application/json',
                    'Authorization: Token ' .$this->token
                ));    
        $result = curl_exec($ch);
        curl_close($ch);

        return $result;
    }

    public function UpdateCurlv2($url ,$data) {
        $ch = curl_init();
        $headers  = [
                    'Content-Type:application/x-www-form-urlencoded',
                    'Authorization: Token '.$this->token
                ];
        
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PATCH");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));           
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($ch);

        $dataResult[] = array(
            'result' => $result,
            'statusCode' => $statusCode
        );

        return $dataResult;
    }

    public function PostCurlv2($url ,$data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));           
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/x-www-form-urlencoded',
            'Authorization: Token '.$this->token
        ));
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($ch);

        $dataResult[] = array(
            'result' => $result,
            'statusCode' => $statusCode
        );

        return $dataResult;
    }

    public function GetCurlv2($url){
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type:application/json',
                    'Authorization: Token '.$this->token
                ));    
        $result = curl_exec($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($ch);
        
        $dataResult[] = array(
            'result' => $result,
            'statusCode' => $statusCode
        );

        return $dataResult;
    }

    public function PostCurlv3($url ,$data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);           
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            'IpUser:'.$this->CI->input->ip_address(),
            'Username:'.$this->CI->session->userdata('user_name')
        ));
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($ch);

        $dataResult[] = array(
            'result' => $result,
            'statusCode' => $statusCode
        );

        return $dataResult;
    }

    public function PutCurlv3($url ,$data) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);           
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type:application/json',
            'IpUser:'.$this->CI->input->ip_address(),
            'Username:'.$this->CI->session->userdata('user_name')
        ));
        $result     = curl_exec ($ch);
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if(!$result){die("Connection Failure");}
        curl_close($ch);

        $dataResult[] = array(
            'result' => $result,
            'statusCode' => $statusCode
        );

        return $dataResult;
    }
}
?>