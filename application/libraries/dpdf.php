<?php defined('BASEPATH') or exit('No direct script access allowed');
/**
 * CodeIgniter DomPDF Library
 *
 * Generate PDF's from HTML in CodeIgniter
 *
 * @packge        CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author        Ardianta Pargo
 * @license        MIT License
 * @link        https://github.com/ardianta/codeigniter-dompdf
 */
require_once 'vendor/autoload.php';

use Dompdf\Dompdf;

class dPdf extends Dompdf
{
    /**
     * PDF filename
     * @var String
     */
    public $filename;
    public function __construct()
    {
        parent::__construct();
        $this->filename = "laporan.pdf";
    }
    /**
     * Get an instance of CodeIgniter
     *
     * @access    protected
     * @return    void
     */
    protected function ci()
    {
        return get_instance();
    }
    /**
     * Load a CodeIgniter view into domPDF
     *
     * @access    public
     * @param    string    $view The view to load
     * @param    array    $data The view data
     * @return    void
     */

    public function load_view($view, $data = array())
    {
	//$options = new Options();
      	//$options->setChroot(/mnt/administrator/megavision);
      	//$options->setDefaultFont('courier');

      	//$this->setOptions($options);
        $html = $this->ci()->load->view($view, $data, $stream = TRUE);
        $this->load_html($html);
        // Render the PDF
        $this->render();
        // Output the generated PDF to Browser
        if ($stream) {
            $this->stream($this->filename, array("Attachment" => false));
            // $dompdf->stream($filename . ".pdf", array("Attachment" => 0));
        } else {
            return $this->output();
        }
    }

    public function load_down($view, $data = array())
    {
        $html = $this->ci()->load->view($view, $data, $stream = TRUE);
        $this->load_html($html);
        // Render the PDF
        $this->render();
        // Output the generated PDF to Browser
        if ($stream) {
            $this->stream($this->filename, array("Attachment" => true));
            // $dompdf->stream($filename . ".pdf", array("Attachment" => 0));
        } else {
            return $this->output();
        }
    }
}
