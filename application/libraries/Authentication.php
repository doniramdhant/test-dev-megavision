<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 */
class Authentication {
	
	var $CI;
	
	function __construct() {
		$this->CI =& get_instance();
		$this->CI->load->model('Cabang_model');
		$this->CI->load->model('Hak_akses_model');
	}
	
	public function authenticate()
	{
		if ($this->CI->session->userdata('identitas') == NULL) {
			redirect(site_url());
		}
	}
	
	public function is_allow_create()
	{
		
	}
	
	public function is_allow_read()
	{
		
	}
	
	public function is_allow_update()
	{
		
	}
	
	public function is_allow_delete()
	{
		
	}
	
	public function check_login(){
		
	}

	public function authenticate_menu($menu_id, $level_id)
	{
		$result = $this->CI->Hak_akses_model->get_by(array('id_modul' => $menu_id, 'id_level' => $level_id));
		if (sizeof($result) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function api_authenticate($api_key)
	{
		$auth = $this->CI->Cabang_model->check_api($api_key)->num_rows();
		if($auth == 0)
		{
			echo '<div style="background-color: #F2DEDE; color: #a94442; padding: 25px; text-align: center;">';
			echo '<strong>Maaf !</strong> Tidak bisa akses API.';
			echo '</div>';
		} 
		else 
		{
			return $auth;
		}
	}
	
}
