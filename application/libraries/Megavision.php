<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
class Megavision
{
	function __construct()
	{
        /*if (production) {
            //$url      = "http://192.168.86.57:8200";
            $url      = "http://192.168.0.21:8200";
            $token    = "Token fec95bf37c16d481eb6930519f63d0591a6c2c39";
        } else {
            $url      = "http://192.168.255.240:8400";
            $token    = "Token 0797e6a8f266dcd20e9a5e2237a6a2322253e438";
        }*/

		$this->obj =& get_instance();
	}
	function is_logged_in()
	{
		if ($this->obj->session) {
			if ($this->obj->session->userdata('logged_in'))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}
	} 
	function login_routine()
	{
        $login_result = FALSE;
        $this->obj->load->library('session');
        $this->obj->load->library('ApiCurl');
		$this->obj->load->helper('url');

        $username = $this->obj->input->post('username');
        $password = $this->obj->input->post('password');

        //$url = $this->obj->url."/global/?search=". $username . "," . md5($password);

        //$result = $this->obj->apicurl->GetCurl($url);
      
        //$data = json_decode($result);
        //var_dump($data[0]);
        //die;
        /*    $arr_result = [];
            foreach($data as $row)
                $arr_result[] = array(
                    'name' => $row->name,
                    'password' => $row->password
                );
            $rr = json_encode($arr_result);
        //var_dump($row->name);
        //die;
        */
        $data['hasil']  = $this->obj->m_user->cek_user();
        $login_result   = FALSE;
        $f_aktif        = FALSE;
	    $ips= $this->obj->input->ip_address();
        $ip = substr($ips,0,7);
//        var_dump($data['hasil']);die();
        $this->obj->load->library('form_validation');
        $this->obj->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->obj->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        if ($this->obj->form_validation->run() == FALSE) {
            
        } else if ($data['hasil'] == null) {
            redirect('login');
        } else {
            //$data['berita']     = json_decode();
            $data['user_name']  = $this->obj->input->post('username');
            $data['password']   = $this->obj->input->post('password');
            $data['group']   = 2;
            $login_result    = TRUE;
            //var_dump($login_result);
            //die;
        if ($login_result==TRUE)
        {

            $newdata = array(
                'user_name' => $data['user_name'],
                'password' => $data['password'],
                'group_id' => $data['group'],
                'login' => TRUE
            );
            $sess=md5(session_id());
            $this->obj->session->set_userdata($newdata);
            $this->obj->session->set_userdata($sess);            
            $query  = $this->obj->db->query("SELECT current_timestamp as c");
            foreach($query->result() as $tes){
                $now      = $tes->c;
            }

            if ($data['group'] == '1') {
                redirect('main_menu/su');
            } else if ($data['group'] == '2') {
                redirect('main_menu/admin');
            } else {
            }
            }
            $this->obj->session->set_flashdata('result_login_suspend', "Akun Anda Suspend");
        	redirect('login');
        }
    }

    function login_routine_mobile()
	{
        $login_result = FALSE;
        $this->obj->load->library('session');
        $this->obj->load->library('ApiCurl');
		$this->obj->load->helper('url');

        $username = $this->obj->input->post('username');
        $password = $this->obj->input->post('password');

       /* $url = $this->obj->url."/global/?search=". $username . "," . md5($password);

        $result = $this->obj->apicurl->GetCurl($url);
      
        $data = json_decode($result);
        //var_dump($data[0]);
        //die;
            $arr_result = [];
            foreach($data as $row)
                $arr_result[] = array(
                    'name' => $row->name,
                    'password' => $row->password
                );
            $rr = json_encode($arr_result);
        //var_dump($row->name);
        //die;*/
        $data['hasil']  = $this->obj->m_user->cek_user();
        $login_result   = FALSE;
        $f_aktif        = FALSE;
	    $ips= $this->obj->input->ip_address();
        $ip = substr($ips,0,7);
//        var_dump($data['hasil']);die();
        $this->obj->load->library('form_validation');
        $this->obj->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->obj->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        if ($this->obj->form_validation->run() == FALSE) {
            
        } else if ($data['hasil'] == null) {
            redirect('login');
        } else {
            //$data['berita']     = json_decode();
            $data['user_name']  = $this->obj->input->post('username');
            $data['password']   = $this->obj->input->post('password');
            $data['group']   = 2;
            $login_result    = TRUE;
            //var_dump($login_result);
            //die;
        if ($login_result==TRUE)
        {

            $newdata = array(
                'user_name' => $data['user_name'],
                'password' => $data['password'],
                'group_id' => $data['group'],
                'login' => TRUE
            );
            $sess=md5(session_id());
            $this->obj->session->set_userdata($newdata);
            $this->obj->session->set_userdata($sess);            
            $query  = $this->obj->db->query("SELECT current_timestamp as c");
            foreach($query->result() as $tes){
                $now      = $tes->c;
            }

            if ($data['group'] == '1') {
                redirect('main_menu/su');
            } else if ($data['group'] == '2') {
                redirect('main_menu/admin_mobile');
            } else {
            }
            }
            $this->obj->session->set_flashdata('result_login_suspend', "Akun Anda Suspend");
        	redirect('login');
        }
    }
}
?>
