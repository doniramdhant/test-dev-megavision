<?php return function ($fontDir, $rootDir) {
return array (
  'sans-serif' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '/ZapfDingbats',
    'bold' => $fontDir . '/ZapfDingbats',
    'italic' => $fontDir . '/ZapfDingbats',
    'bold_italic' => $fontDir . '/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '/Symbol',
    'bold' => $fontDir . '/Symbol',
    'italic' => $fontDir . '/Symbol',
    'bold_italic' => $fontDir . '/Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '/DejaVuSans-Bold',
    'bold_italic' => $fontDir . '/DejaVuSans-BoldOblique',
    'italic' => $fontDir . '/DejaVuSans-Oblique',
    'normal' => $fontDir . '/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '/DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '/DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '/DejaVuSansMono-Oblique',
    'normal' => $fontDir . '/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '/DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '/DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '/DejaVuSerif-Italic',
    'normal' => $fontDir . '/DejaVuSerif',
  ),
  'open sans medium' => array(
    '600' => $fontDir . '/open_sans_medium_600_13f914398c1e1e655de4e43b153a84a0',
  ),
  'opensanssemi' => array(
    'normal' => $fontDir . '/opensanssemi_normal_13f914398c1e1e655de4e43b153a84a0',
  ),
  'seguisb' => array(
    'normal' => $fontDir . '/seguisb_normal_76d1b505b62e2c6b20b08baa1d56291b',
  ),
  'arialmed' => array(
    'normal' => $fontDir . '/arialmed_normal_c6770e9f0babe23d84c593e780c7a462',
  ),
  'verdanasb' => array(
    'normal' => $fontDir . '/verdanasb_normal_52502235f2500e40f56f57dc7dd691e1',
  ),
  'tahomareg' => array(
    'normal' => $fontDir . '/tahomareg_normal_0b4b3f33e40c70770b29079573df2592',
  ),
  'tahomabd' => array(
    'normal' => $fontDir . '/tahomabd_normal_6655cf9945b5074a1c5d96a22f227f32',
  ),
  'tahoma' => array(
    'normal' => $fontDir . '/tahoma_normal_a04dbaa3c9669f77e87b2420c774823a',
  ),
  'tahoma_0' => array(
    'normal' => $fontDir . '/tahoma_0_normal_7a1a029282bef9d5c9007f878b3edc30',
  ),
  'helveticamed' => array(
    'normal' => $fontDir . '/helveticamed_normal_3568581e6b2d218320e0914247a72525',
  ),
  'opensanssb' => array(
    'normal' => $fontDir . '/opensanssb_normal_2974467de7edc873ab454cfb270d14f7',
  ),
);
}; ?>